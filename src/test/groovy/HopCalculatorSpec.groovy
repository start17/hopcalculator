import domain.Graph
import domain.Node
import spock.lang.Specification

class HopCalculatorSpec extends Specification {

    Set<Node> nodes = [
            new Node('A', ['B', 'C'] as Set),
            new Node('B', ['A', 'C', 'D', 'E'] as Set),
            new Node('C', ['A', 'B', 'E'] as Set),
            new Node('D', ['B', 'E'] as Set),
            new Node('E', ['B', 'C', 'D'] as Set),
            new Node('F', [] as Set)
    ] as Set

    Graph graph = new Graph(nodes)

    HopCalculator hopCalculator = new HopCalculator(graph)

    def 'should return least number of hops between source and destination node'() {

        when:
        int numberOfHopes = hopCalculator.calculateLeastNumberOfHopesBetween(sourceNodeName, destinationNodeName)

        then:
        numberOfHopes == expectedNumberOfHopes

        where:
        sourceNodeName | destinationNodeName | expectedNumberOfHopes
        'A'            | 'E'                 | 2
        'A'            | 'A'                 | 0
        'A'            | 'F'                 | -1
        'C'            | 'D'                 | 2
    }
}
