import domain.Graph;

public class HopCalculator {

    private final Graph graph;

    public HopCalculator(Graph graph) {
        this.graph = graph;
    }

    public int calculateLeastNumberOfHopesBetween(String sourceNodeName, String destinationNodeName) {

        return graph.calculateLeastNumberOfHopesBetween(sourceNodeName, destinationNodeName);
    }
}
