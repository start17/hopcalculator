package domain;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.Set;

public class Node {

    private final String name;
    private final Set<String> adjacentNodeNames;

    public Node(String name, Set<String> adjacentNodeNames) {
        this.name = name;
        this.adjacentNodeNames = adjacentNodeNames;
    }


    @SuppressWarnings("EqualsWhichDoesntCheckParameterClass")
    @Override
    public boolean equals(Object that) {

        return EqualsBuilder.reflectionEquals(this, that);
    }

    @Override
    public int hashCode() {

        return HashCodeBuilder.reflectionHashCode(this);
    }

    public String getName() {
        return name;
    }

    public Set<String> getAdjacentNodeNames() {
        return adjacentNodeNames;
    }
}
