package domain;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Graph {

    private final Map<String, Node> nodes;

    public Graph(Set<Node> nodeSet) {

        nodes = new HashMap<>(nodeSet.size());
        for (Node node : nodeSet) {
            nodes.put(node.getName(), node);
        }
    }

    public int calculateLeastNumberOfHopesBetween(String sourceNodeName, String destinationNodeName) {

        Set<String> visitedNodes = new HashSet<>(nodes.size());
        Map<String, Integer> nodeHopCountMap = new HashMap<>(nodes.size());

        String nodeName = sourceNodeName;
        int nodeHopCount = 0;

        while (true) {
            if (nodeName.equals(destinationNodeName)) {
                return nodeHopCount;
            }

            //put adjacent nodes into nodeHopCountMap
            int newAdjacentNodeHopCount = nodeHopCount + 1;
            Node node = nodes.get(nodeName);
            for (String adjacentNodeName : node.getAdjacentNodeNames()) {

                if (!visitedNodes.contains(adjacentNodeName)) {
                    Integer adjacentNodeHopCount = nodeHopCountMap.get(adjacentNodeName);
                    if (adjacentNodeHopCount == null || adjacentNodeHopCount > newAdjacentNodeHopCount) {
                        nodeHopCountMap.put(adjacentNodeName, newAdjacentNodeHopCount);
                    }
                }
            }

            //mark node as visited
            visitedNodes.add(nodeName);

            if (nodeHopCountMap.isEmpty()) {
                return -1; //there is no route to destination node so return -1
            }

            //next node to process
            nodeName = findNodeNameWithSmallestHopCount(nodeHopCountMap, destinationNodeName);
            nodeHopCount = nodeHopCountMap.remove(nodeName);
        }
    }

    @SuppressWarnings("ConstantConditions")
    private String findNodeNameWithSmallestHopCount(Map<String, Integer> nodeHopCountMap, String destinationNodeName) {

        Map.Entry<String, Integer> entryWithSmallestHopCount = null;

        for (Map.Entry<String, Integer> entry : nodeHopCountMap.entrySet()) {
            if (entryWithSmallestHopCount == null) {
                entryWithSmallestHopCount = entry;
            } else {
                if (entryWithSmallestHopCount.getValue() > entry.getValue()) {
                    entryWithSmallestHopCount = entry;
                } else if (entryWithSmallestHopCount.getValue().equals(entry.getValue()) && entry.getKey().equals(destinationNodeName)) {
                    entryWithSmallestHopCount = entry;
                }
            }
        }

        return entryWithSmallestHopCount.getKey();
    }
}
